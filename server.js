
var express = require('express');
var app = express();
app.use(express.static('public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/public/inicio.html');
});

app.listen(4000);
console.log("Servidor Express escuchando en modo 4000.");
